package com.example.prueba3.presentador;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prueba3.R;
import com.example.prueba3.model.Auto;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DetalleRuta extends AppCompatActivity {

    ArrayList<Auto> spinerDataList = new ArrayList<>();
    ArrayAdapter<Auto> adapterAuto;
    Spinner spinner;
    ValueEventListener listener;

    private EditText mEditTextHllegada;
    private Button mButtonFinalizarR;
    private TextView mTextViewnombre;
    private TextView mTextViewemail;
    private TextView mTextViewdireccion;

    private double lat = 0;
    private double lon = 0;
    private String dire = "";
    private int asientos = 0;
    private String Hllegada = "";
    private String soat = "";
    private String placa = "";
    private String marca = "";

    FirebaseAuth nAuth;
    DatabaseReference mDatabase;
    private String name = "";
    private String email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_ruta);

        InicializarVaribles();

        InicializarFirebase();

        mButtonFinalizarR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegistrarRutaFirebase();
            }
        });

        spinner.setAdapter(adapterAuto);

        recibirDatosRuta();

        getUserInfo();

        RetrieveData();

    }

    private void RegistrarRutaFirebase() {
        Hllegada = mEditTextHllegada.getText().toString();

        if (!Hllegada.isEmpty()) {
            RegistrarRuta();

        } else {
            Toast.makeText(DetalleRuta.this, "Complete los Datos", Toast.LENGTH_SHORT).show();
        }
    }

    private void InicializarFirebase() {
        nAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    private void InicializarVaribles() {
        mEditTextHllegada = (EditText) findViewById(R.id.txtHllegada);
        mButtonFinalizarR = (Button) findViewById(R.id.btnfinalizarR);
        mTextViewdireccion = (TextView) findViewById(R.id.txtdireccionC);
        mTextViewemail = (TextView) findViewById(R.id.txtemailconductor);
        mTextViewnombre = (TextView) findViewById(R.id.txtconductor);
        spinner = (Spinner) findViewById(R.id.spinner);
    }

    private void RegistrarRuta() {

        Map<String, Object> ruta = new HashMap<>();
        ruta.put("conductor", name);
        ruta.put("email", email);
        ruta.put("direccion", dire);
        ruta.put("latitud", lat);
        ruta.put("longitude", lon);
        ruta.put("Hllegada", Hllegada);
        ruta.put("asientos", asientos);
        ruta.put("soat", soat);
        ruta.put("placa", placa);
        ruta.put("marca", placa);


        mDatabase.child("Ruta").push().setValue(ruta);

        Toast.makeText(this, "Ruta resgitrada", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(DetalleRuta.this, Conductor.class);
        startActivity(i);
    }

    private void getUserInfo() {
        String id = nAuth.getCurrentUser().getUid();
        mDatabase.child("Users").child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    name = dataSnapshot.child("name").getValue().toString();
                    email = dataSnapshot.child("email").getValue().toString();

                    mTextViewnombre.setText(name);
                    mTextViewemail.setText(email);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void RetrieveData() {

        mDatabase.child("Users").child(nAuth.getCurrentUser().getUid())
                .child("Auto")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        spinerDataList.clear();
                        //adapterAuto.notifyDataSetChanged();

                        for (DataSnapshot item : dataSnapshot.getChildren()) {
                            Auto oAuto = item.getValue(Auto.class);

                            spinerDataList.add(oAuto);
                            adapterAuto = new ArrayAdapter<Auto>(DetalleRuta.this, android.R.layout.simple_spinner_dropdown_item, spinerDataList);
                            spinner.setAdapter(adapterAuto);

                            marca = oAuto.getMarca();
                            soat = oAuto.getSoat();
                            placa = oAuto.getPlaca();
                            asientos = oAuto.getAsientos();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }


    private void recibirDatosRuta() {
        Bundle extras = getIntent().getExtras();
        lat = extras.getDouble("latitud");
        lon = extras.getDouble("longitud");
        dire = extras.getString("direccion");
        mTextViewdireccion.setText(dire);
    }


}
