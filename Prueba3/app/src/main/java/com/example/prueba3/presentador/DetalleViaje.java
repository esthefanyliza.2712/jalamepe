package com.example.prueba3.presentador;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prueba3.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;


public class DetalleViaje extends AppCompatActivity {
    private String conductor;
    private String direccion;
    private String email;
    private String hora;
    private String costo;
    private String soat;
    private String placa;
    private String asientosInicio, asientosFinsl;
    private int y=0, x=0;
    private int TotalAsiento=0;
    private double longitud, latitud;
    private String nombrePasajero;
    private String emailPasajero;
    private TextView TextViewConductor;
    private TextView TextViewEmail;
    private TextView TextViewDireccion;
    private TextView TextViewMarca;
    private TextView TextViewPlaca;
    private TextView TextViewSoat;
    private TextView TextViewhora;
    private TextView TextViewCosto;
    private EditText EditTextAsientos;
    private TextView TextViewMapa;
    private Button ButtonRegistroViaje;
    FirebaseAuth nAuth;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_viaje);

        nAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        TextViewConductor = (TextView) findViewById(R.id.textname);
        TextViewEmail = (TextView) findViewById(R.id.textEmail);
        TextViewDireccion = (TextView) findViewById(R.id.textDireccion);
        TextViewPlaca = (TextView) findViewById(R.id.textPlaca);
        TextViewSoat = (TextView) findViewById(R.id.textSoat);
        TextViewhora = (TextView) findViewById(R.id.textHora);
        TextViewCosto = (TextView) findViewById(R.id.textCosto);
        EditTextAsientos = (EditText) findViewById(R.id.textAsientos);
        TextViewMapa = (TextView) findViewById(R.id.textmapa);
        ButtonRegistroViaje = (Button) findViewById(R.id.btnRegistarViaje);
        onclick();
        onclickRegitroViaje();
        listarDatosViaje();
        getUserInfo();
    }

    private void onclickRegitroViaje() {
        ButtonRegistroViaje.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Map<String,Object> subcripcionV = new HashMap<>();
                subcripcionV.put("pasajero", nombrePasajero);
                subcripcionV.put("emailPasajero", emailPasajero);
                subcripcionV.put("conductor", conductor );
                subcripcionV.put("emailConductor",email);
                subcripcionV.put("latitud", latitud);
                subcripcionV.put("longitud", longitud);
                subcripcionV.put("direccion", direccion);
                subcripcionV.put("costo", costo);
                subcripcionV.put("AsientosIncio",x);
                subcripcionV.put("AsientosFinal",y);
                subcripcionV.put("TotalAsientos",TotalAsiento);
                subcripcionV.put("hora", hora);
                subcripcionV.put("soat", soat);
                subcripcionV.put("placa",placa);

                mDatabase.child("subcripcionV").push().setValue(subcripcionV);
                Toast.makeText(DetalleViaje.this, "Viaje Guardado", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(DetalleViaje.this, Pasajero.class);
                startActivity(i);
            }
        });
    }

    private void onclick() {
        TextViewMapa.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(DetalleViaje.this, MapaViaje.class);
            i.putExtra("latitud",latitud);
            i.putExtra("longitude",longitud);
            i.putExtra("direccion",direccion);
            startActivity(i);
        }
    });

    }

    private void listarDatosViaje() {
        Bundle extras = getIntent().getExtras();
        conductor = extras.getString("conductor");
        email = extras.getString("email");
        direccion = extras.getString("direccion");
        hora = extras.getString("Hllegada");
        costo = extras.getString("costo");
        placa = extras.getString("placa");
        soat = extras.getString("soat");
        asientosInicio = extras.getString("nasientos");
        latitud = extras.getDouble("latitud");
        longitud = extras.getDouble("longitude");

        TextViewConductor.setText("Conductor: "+conductor);
        TextViewEmail.setText("Email :"+email);
        TextViewDireccion.setText("Punto de Recojo :"+direccion);
        TextViewhora.setText("Hora :"+hora);
        TextViewCosto.setText("Precio :"+costo);
        TextViewPlaca.setText("Placa del Auto :"+placa);
        TextViewSoat.setText("Soat"+ soat);
        EditTextAsientos.setText(asientosInicio);

        // operar asientos

        asientosFinsl = EditTextAsientos.getText().toString();
        x = Integer.parseInt(asientosInicio);
        y = Integer.parseInt(asientosFinsl);

        int z = (x -y);
        TotalAsiento = z;
    }

    private void getUserInfo(){
        String id= nAuth.getCurrentUser().getUid();
        mDatabase.child("Users").child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    nombrePasajero = dataSnapshot.child("name").getValue().toString();
                    emailPasajero = dataSnapshot.child("email").getValue().toString();}
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
