package com.example.prueba3.presentador;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.prueba3.R;
import com.example.prueba3.model.ruta;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class BuscarRuta extends AppCompatActivity {
    private List<ruta> lista =new ArrayList<ruta>();
    private ListView Lista_Ruta;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    ArrayAdapter<ruta> arrayAdapterRuta;

    ruta rutaSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_ruta);

        Lista_Ruta = (ListView) findViewById(R.id.ListaRutas);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        listadatos();
        Lista_Ruta.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                rutaSelected=(ruta) parent.getItemAtPosition(position);
                Intent visorDetalle = new Intent(view.getContext(), DetalleViaje.class);
                visorDetalle.putExtra("conductor",rutaSelected.getConductor());
                visorDetalle.putExtra("email",rutaSelected.getEmail());
                visorDetalle.putExtra("direccion",rutaSelected.getDireccion());
                visorDetalle.putExtra("Hllegada",rutaSelected.getHllegada());
                visorDetalle.putExtra("costo",rutaSelected.getCosto());
                visorDetalle.putExtra("soat",rutaSelected.getSoat());
                visorDetalle.putExtra("placa",rutaSelected.getPlaca());
                visorDetalle.putExtra("nasientos",rutaSelected.getNasientos());
                visorDetalle.putExtra("latitud",rutaSelected.getLatitud());
                visorDetalle.putExtra("longitude",rutaSelected.getLongitude());


                startActivity(visorDetalle);
            }
        });


    }

    private void listadatos() {
        mDatabase.child("viaje").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lista.clear();
                for (DataSnapshot objSnapshot: dataSnapshot.getChildren()){
                    final ruta r=objSnapshot.getValue(ruta.class);
                    lista.add(r);

                    arrayAdapterRuta =new ArrayAdapter<ruta>(BuscarRuta.this,android.R.layout.simple_list_item_1, lista);
                    Lista_Ruta.setAdapter(arrayAdapterRuta);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
