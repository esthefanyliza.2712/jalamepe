package com.example.prueba3.presentador;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.prueba3.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    private EditText mEditTextEmail;
    private EditText mEditTextPassword;
    private Button mButtonLogin;
    private Button mButtonResetPassword;
    private String email = "";
    private String password = "";
    private int rol;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        InicializarVariables();

        InicializarFirebase();

        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LoginUsuario();
            }
        });

        mButtonResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ResetPasswordActivity.class));
            }
        });
    }


    private void LoginUsuario() {
        email = mEditTextEmail.getText().toString();
        password = mEditTextPassword.getText().toString();

        if (!email.isEmpty() && !password.isEmpty()) {
            LoginUser();
        } else {
            Toast.makeText(LoginActivity.this, "Complete los compos", Toast.LENGTH_SHORT).show();
        }
    }


    private void InicializarFirebase() {
        mAuth = FirebaseAuth.getInstance();
    }

    private void InicializarVariables() {
        mEditTextEmail = (EditText) findViewById(R.id.txtcorreo);
        mEditTextPassword = (EditText) findViewById(R.id.txtpassword);
        mButtonLogin = (Button) findViewById(R.id.btnLogin);
        mButtonResetPassword = (Button) findViewById(R.id.btnsendToResetPassword);
    }

    private void LoginUser() {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {
                    startActivity(new Intent(LoginActivity.this, ProfileActivity.class));
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, "No se puede iniciar sesion, comprueba los datos", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
