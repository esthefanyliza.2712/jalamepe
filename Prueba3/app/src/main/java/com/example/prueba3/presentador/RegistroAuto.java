package com.example.prueba3.presentador;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.prueba3.R;
import com.example.prueba3.model.Auto;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RegistroAuto extends AppCompatActivity {
    private List<Auto> listAuto =new ArrayList<Auto>();
    ArrayAdapter<Auto> arrayAdapterAuto;

    private EditText EditTextMarca;
    private EditText EditTextPlaca;
    private EditText EditTextSoat;
    private EditText EditTextAsientos;
    private ListView List_Autos;

    Auto autoSelected;
    DatabaseReference mDatabase;
    FirebaseDatabase firebaseDatabase;
    private String marca="";
    private String placa="";
    private String soat="";
    private String id= "";
    private int asientos=0;

    private Button mUploadBtn;
    private Button mChoose;
    private ImageView mImageview;
    private Uri filepathA;
    private final int PICK_IMAGE_REQUEST = 71;

    FirebaseStorage storage;
    StorageReference storageReference;

    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_auto);

        InicializarVariables();

        InicializarFirebase();

        listadatos();

        mChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });



        mUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImagen ();
            }
        });


        List_Autos.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                autoSelected=(Auto) parent.getItemAtPosition(position);
                EditTextMarca.setText(autoSelected.getMarca());
                EditTextPlaca.setText(autoSelected.getPlaca());
                EditTextSoat.setText(autoSelected.getSoat());
                EditTextAsientos.setText(autoSelected.getAsientos());
            }
        });

    }

    private void InicializarFirebase() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        firebaseDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
    }

    private void InicializarVariables() {
        EditTextMarca = (EditText) findViewById(R.id.editMarca);
        EditTextPlaca = (EditText) findViewById(R.id.editPlaca);
        EditTextSoat = (EditText) findViewById(R.id.editSoat);
        EditTextAsientos = (EditText) findViewById(R.id.editAaienctos);
        mChoose = (Button) findViewById(R.id.btnEscoge);
        mUploadBtn = (Button) findViewById(R.id.btnSubir);
        mImageview = (ImageView) findViewById(R.id.SubirImagen);
        List_Autos = (ListView) findViewById(R.id.listAuto);
    }


    private void uploadImagen() {

        if (filepathA !=  null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Subiendo...");
            progressDialog.show();

            final StorageReference rf = storageReference.child("autoimagenes/" + UUID.randomUUID().toString());
            rf.putFile(filepathA)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            rf.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Uri downloadUrl = uri;
                                    Log.e("URLfOTO1"," : "+downloadUrl.toString()); //es tu foto
                                }
                            });
                            progressDialog.dismiss();
                            Toast.makeText(RegistroAuto.this, "Uploaded", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(RegistroAuto.this, "Fails", Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                    progressDialog.setMessage("Uploaded" + (int)progress+"%");
                }
            });
        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(intent.ACTION_GET_CONTENT);
        startActivityForResult(intent.createChooser(intent,"select picture"),PICK_IMAGE_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){

            filepathA = data.getData();
            try {
                Bitmap bitmap   = MediaStore.Images.Media.getBitmap(getContentResolver(), filepathA);
                mImageview.setImageBitmap(bitmap);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

        }
    }


    private void listadatos() {
        mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).
                child("Auto").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listAuto.clear();
                for (DataSnapshot objSnapshot: dataSnapshot.getChildren()){
                    Auto a=objSnapshot.getValue(Auto.class);
                    listAuto.add(a);

                    arrayAdapterAuto =new ArrayAdapter<Auto>(RegistroAuto.this,android.R.layout.simple_list_item_1, listAuto);
                    List_Autos.setAdapter(arrayAdapterAuto);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_auto,menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        marca = EditTextMarca.getText().toString();
        placa = EditTextPlaca.getText().toString();
        soat = EditTextSoat.getText().toString();
        asientos = Integer.parseInt(EditTextAsientos.getText().toString());

        switch (item.getItemId()){
            case R.id.icon_add:{
                if(marca.equals("")||placa.equals("")||soat.equals("")||asientos==0){
                    Toast.makeText(this, "Ingresar Datos", Toast.LENGTH_SHORT).show();
                    validacion();
                }
                else{
                    Auto a= new Auto();
                    a.setId(UUID.randomUUID().toString());
                    a.setMarca(marca);
                    a.setPlaca(placa);
                    a.setSoat(soat);
                    a.setAsientos(asientos);
                    mDatabase.child("Users").child(mAuth.getCurrentUser().getUid())
                            .child("Auto").child(a.getId()).setValue(a);
                    Toast.makeText(this,"Agregar",Toast.LENGTH_LONG).show();
                    limpiarcajas();

                }
                break;
            }
            case R.id.icon_save:{
                Auto a =new Auto();
                a.setId(autoSelected.getId());
                a.setMarca(EditTextMarca.getText().toString().trim());
                a.setPlaca(EditTextPlaca.getText().toString().trim());
                a.setSoat(EditTextSoat.getText().toString().trim());
                a.setAsientos(Integer.parseInt(EditTextAsientos.getText().toString().trim()));
                mDatabase.child("Users").child(mAuth.getCurrentUser().getUid())
                        .child("Auto").child(a.getId()).setValue(a);

                Toast.makeText(this,"Guardar",Toast.LENGTH_LONG).show();
                limpiarcajas();
                break;

            }
            case R.id.icon_delete:{
                Auto a =new Auto();
                a.setId(autoSelected.getId());
                mDatabase.child("Users").child(mAuth.getCurrentUser().getUid())
                        .child("Auto").child(a.getId()).removeValue();
                Toast.makeText(this,"Delete",Toast.LENGTH_LONG).show();
                limpiarcajas();
                break;
            }
            default:break;

        }
        return true;
    }

    private void limpiarcajas() {
        EditTextMarca.setText("");
        EditTextPlaca.setText("");
        EditTextSoat.setText("");
        EditTextAsientos.setText("");
    }

    private void validacion() {

        if (marca.equals("")) {
            EditTextMarca.setError("Required");
        } else if (placa.equals("")) {
            EditTextPlaca.setError("Required");
        } else if (soat.equals("")) {
            EditTextSoat.setError("Required");
        } else if (asientos==0) {
            EditTextAsientos.setError("Required");
        }
    }
}

