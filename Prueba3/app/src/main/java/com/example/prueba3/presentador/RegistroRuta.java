package com.example.prueba3.presentador;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prueba3.GPSTracker;
import com.example.prueba3.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class RegistroRuta extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private TextView tvDireccion;
    private double lat = 0, lng = 0;
    private Button mButtonGuardarDire;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_ruta);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mButtonGuardarDire = (Button) findViewById(R.id.btnguardardireccion);
        initView();

    }

    private void  initView(){

        tvDireccion = (TextView) findViewById(R.id.tvDireccion);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //permisos

        if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(RegistroRuta.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RegistroRuta.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 111);
            return;
        }

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.setOnMarkerClickListener(this);
        obtenerUbicacion();
    }

    // metodo para obtener ubicacion
    private void obtenerUbicacion(){
        GPSTracker gps = new GPSTracker(getApplicationContext());
        lat =  gps.getLatitude();
        lng = gps.getLongitude();
        agregarMarkador(lat,lng);
    }

    private void agregarMarkador(double lat, double lng){
        LatLng coordenadas = new LatLng(lat,lng); // obtenemos coordenadas por la lat y lng

        // volver a validar si tiene persmisos
        if (ActivityCompat.checkSelfPermission(RegistroRuta.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }else{
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(coordenadas)
                    .title("Ubicacion")
                    .draggable(true)
                    .icon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.ic_marker)));

            marker.showInfoWindow();

            mMap.setMyLocationEnabled(true);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(coordenadas));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng),15.0f)); //zoom del mapa

            // cuando cargarla primera vez te muestra tu direccion
            tvDireccion.setText(obtenerDireccion(lat,lng));

            // metodo para mover el marker
            mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {
                }

                @Override
                public void onMarkerDrag(Marker marker) {
                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    //TODO CADA VEZ QUE MUEVAS EL MARKER ENTRA A ESTE METODO DE GOOGLE Y OBTIENE LA NUEVA UBICACION
                    // onMarkerDragEnd  "End" cuando terminas de soltar el market al moven ira al metodo de obtener la nueva direcion
                    // aqui te dara otras coordenadas
                    LatLng posicion  =  marker.getPosition();
                    Log.e("DIRECCION CHANGE",""+obtenerDireccion(posicion.latitude,posicion.longitude));
                    tvDireccion.setText(obtenerDireccion(posicion.latitude,posicion.longitude));

                    final String direccion = obtenerDireccion(posicion.latitude,posicion.longitude);
                    final double latitude = posicion.latitude;
                    final double longitud = posicion.longitude;
                    mButtonGuardarDire.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (direccion!=null && latitude!=0 && longitud!=0){
                                Intent i = new Intent(RegistroRuta.this, DetalleRuta.class);
                                i.putExtra("latitud",latitude);
                                i.putExtra("longitud",longitud);
                                i.putExtra("direccion",direccion);
                                startActivity(i);

                                Toast.makeText(RegistroRuta.this, "Direccion guardada", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            });
        }
    }


    private String obtenerDireccion(Double latitud, Double longitud){
        String direccion = "";

        Geocoder geo = new Geocoder(RegistroRuta.this, Locale.getDefault());
        List<Address> addresses =  null;

        try {
            addresses = geo.getFromLocation(latitud,longitud,1);
            if(!addresses.isEmpty()){
                if(addresses.size()>0){
                    direccion = addresses.get(0).getAddressLine(0);
                }
            }else{
                Log.e("ERROR","ocurrio un error al obtener la direccion");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return  direccion;
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

}
