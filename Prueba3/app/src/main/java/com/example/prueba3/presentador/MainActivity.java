package com.example.prueba3.presentador;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.prueba3.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


public class MainActivity extends AppCompatActivity {

    private EditText mEditTextName;
    private EditText mEditTextEmail;
    private EditText mEditTextPassword;
    private EditText mEditTextTelefono;
    private EditText mEditTextDNI;
    private EditText mEditTextApellido;
    private Button mButtonRegister;
    private Button mButtonSendtoLogin;
    private String name="";
    private String email="";
    private String password="";
    private String telefono= "";
    private String dni= "";
    private String apellido="";
    private Button mUploadBtn;
    private Button mChoose;
    private ImageView mImageview;
    private Uri filepath;
    private final int PICK_IMAGE_REQUEST = 71;
    FirebaseStorage storage;
    StorageReference storageReference;
    FirebaseAuth nAuth;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        variables();
        Firebase();

        mChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });

        mUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImagen ();
            }
        });


        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegistroUsuario();
            }
        });

        mButtonSendtoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });
    }

    private void RegistroUsuario() {
        name = mEditTextName.getText().toString();
        email = mEditTextEmail.getText().toString();
        password = mEditTextPassword.getText().toString();
        dni = mEditTextDNI.getText().toString();
        telefono = mEditTextTelefono.getText().toString();
        apellido = mEditTextApellido.getText().toString();

        ///validar los campos
        if(!name.isEmpty() && !email.isEmpty() && !password.isEmpty()&&
                !telefono.isEmpty()&& !dni.isEmpty()&& !apellido.isEmpty()){
            RegistroUsuarioFirebase();

        }
        else{
            validacion();
            Toast.makeText(MainActivity.this, "Debe completar los campos", Toast.LENGTH_SHORT).show();
        }
    }

    private void Firebase() {
        nAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
    }

    private void variables() {
        mEditTextName = findViewById(R.id.txtnombre);
        mEditTextEmail=findViewById(R.id.txtcorreo);
        mEditTextPassword= findViewById(R.id.txtpassword);
        mEditTextTelefono = findViewById(R.id.txtTelefono);
        mEditTextDNI = findViewById(R.id.txtDni);
        mEditTextApellido = findViewById(R.id.txtapellido);
        mButtonRegister=findViewById(R.id.btnregistar);
        mButtonSendtoLogin=findViewById(R.id.btnSendtoLogin);
        mImageview = (ImageView) findViewById(R.id.SubirImagen);
        mChoose = (Button) findViewById(R.id.btnEscoge);
        mUploadBtn = (Button) findViewById(R.id.btnSubir);
    }

    private void uploadImagen() {

        if (filepath !=  null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Subiendo...");
            progressDialog.show();

            final StorageReference riversRef = storageReference.child("imagenes/" + UUID.randomUUID().toString());
            riversRef.putFile(filepath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                         public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Uri downloadUrl = uri;
                                    Log.e("URLfOTO"," : "+downloadUrl.toString()); //es tu foto
                                }
                            });
                            progressDialog.dismiss();
                            Toast.makeText(MainActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Fails", Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                    progressDialog.setMessage("Uploaded" + (int)progress+"%");
                }
            });
        }
    }

    private void chooseImage() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(intent.ACTION_GET_CONTENT);
        startActivityForResult(intent.createChooser(intent,"select picture"),PICK_IMAGE_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){

            filepath = data.getData();
            try {
                Bitmap bitmap   = MediaStore.Images.Media.getBitmap(getContentResolver(),filepath);
                mImageview.setImageBitmap(bitmap);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

        }
    }


    private void RegistroUsuarioFirebase() {
        nAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    int rol = 1;

                    Map<String, Object> map =new HashMap<>();
                    map.put("name",name);
                    map.put("email",email);
                    map.put("password",password);
                    map.put("telefono", telefono);
                    map.put("dni", dni);
                    map.put("apellido",apellido);
                    map.put("rol",rol);

                    String id = nAuth.getCurrentUser().getUid();
                    mDatabase.child("Users").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task2) {
                            if(task2.isSuccessful()){
                                startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                                finish();
                            }else{
                                Toast.makeText(MainActivity.this,"No se pudo crear los datos correctamente",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    Toast.makeText(MainActivity.this, "Usuario no registrado",Toast.LENGTH_SHORT ).show();
                }
            }
        });

        //limpiarcajas();
    }


    private void limpiarcajas() {
        mEditTextName.setText("");
        mEditTextEmail.setText("");
        mEditTextPassword.setText("");
    }

    private void validacion() {

        if (name.equals("")) {
            mEditTextName.setError("Required");
        }
        else if (apellido.equals("")) {
            mEditTextApellido.setError("Required");
        }
        else if (dni.equals("")) {
            mEditTextDNI.setError("Required");
        }
        else if (dni.length()!=8) {
            mEditTextDNI.setError("Required");
        }
        else if (telefono.equals("")) {
            mEditTextTelefono.setError("Required");
        }
        else if (telefono.length()!=9) {
            mEditTextTelefono.setError("Required");
        }
        else if (email.equals("")) {
            mEditTextEmail.setError("Required");
        }
        else if (password.equals("")) {
            mEditTextPassword.setError("Required");
        }
        else if (password.length()<=6) {
            mEditTextPassword.setError("Required");
            Toast.makeText(MainActivity.this, "El pasword debe tener 6 caracteres",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        if(nAuth.getCurrentUser() != null){
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
            finish();
        }
    }
}





