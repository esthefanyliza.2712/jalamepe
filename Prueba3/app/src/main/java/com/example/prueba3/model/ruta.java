package com.example.prueba3.model;

public class ruta {
    private String conductor, email, password,
            placa, soat, direccion, costo,
            Nasientos, Hsalida, Hllegada;
    private double latitud, longitude;

    public ruta() {
    }

    public String getConductor() {
        return conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getSoat() {
        return soat;
    }

    public void setSoat(String soat) {
        this.soat = soat;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }

    public String getNasientos() {
        return Nasientos;
    }

    public void setNasientos(String nasientos) {
        Nasientos = nasientos;
    }

    public String getHsalida() {
        return Hsalida;
    }

    public void setHsalida(String hsalida) {
        Hsalida = hsalida;
    }

    public String getHllegada() {
        return Hllegada;
    }

    public void setHllegada(String hllegada) {
        Hllegada = hllegada;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return  "Conductor :" + conductor +"\n"+
                "Direccion :"+direccion+ "\n"+
                "Placa :"+placa+"\n"+
                "Soat :"+soat;
    }


}
